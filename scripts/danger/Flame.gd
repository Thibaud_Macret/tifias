#Made by Xzing, project Tifias
extends Area2D

#a dangerous item which moves slowly

signal death

var tempo #the time between two moves
var timer = 0
var fire_range #the number of moves
var head #will change the sprite
var vect = Vector2()
var mvfe

func init(x_dir, y_dir, is_first, init_tempo, init_fire_range):
	vect.x = 32*x_dir #it will moves a square each time 
	vect.y = 32*y_dir
	head = is_first 
	tempo = init_tempo 
	fire_range = init_fire_range 

func _ready():
	mvfe=self.connect("death",get_parent().get_parent(),"death_trigger")
	#need a parent with a death_parent (death_grand_parent)
	select_sprite()
	global_position.x+=vect.x/4 #in order to look clean with the flamthrower
	global_position.y+=vect.y/4

func select_sprite():
	if (head):
		$Sprite.frame+=1
	if(vect.y):
		$Sprite.rotation_degrees = (vect.y/32)*90
	elif (vect.x == -32):
		$Sprite.rotation_degrees = 180

func _on_Flame_body_entered(body):
	self.emit_signal("death")
	mvfe=body

func _physics_process(delta):
	if(fire_range): #if it needs to move
		timer+=delta
		if (timer>tempo):
			timer = 0
			fire_range-=1
			global_position.x+=vect.x #ignores walls
			global_position.y+=vect.y

func _on_Activation_zone_body_exited(body): #when far from the player
	queue_free()
	mvfe = body