#Made by Xzing, project Tifias
extends Area2D

#A deadly object wich change sprite for one second when triggered

signal death

var timer = 0
var mvfe

func _ready():
	mvfe=self.connect("death",get_parent(),"death_trigger") #need a death_parent

func _on_Wall_spikes_body_entered(body):
	self.emit_signal("death")
	$Sprite.frame = 54
	mvfe=body

func _physics_process(delta):
	timer+=delta
	if (timer>1):
		timer = 0
		$Sprite.frame = 53