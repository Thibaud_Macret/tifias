#Made by Xzing, project Tifias
extends Node2D

#The parent node of all gems

var sound1_timer = 0
var sound10_timer = 0
var sound100_timer = 0

signal actualisation_display()  #NEED TO BE CONNECTED TO SCORE NODE

func loot_gem(value):
	(points.nb_points) +=  value
	gest_sound(value)
	self.emit_signal("actualisation_display")

func gest_sound(points):
	match points:
		1:
			sound1_timer = 33 #number of frames the sound lasts
			$Sound1.play()
		10 :
			sound10_timer = 33
			$Sound10.play()
		100 :
			sound100_timer = 40
			$Sound100.play()

func _physics_process(delta):
	sound1_timer=gest_timer(sound1_timer,delta)
	sound10_timer=gest_timer(sound10_timer,delta)
	sound100_timer=gest_timer(sound100_timer,delta)
	gest_stop()

func gest_timer(timer,delta):
	if(timer<0):
		timer = 0
	if(timer):
		timer-=100*delta #count in cs
	return timer

func gest_stop():
	if (sound1_timer==0):
		$Sound1.stop()	
	if (sound10_timer==0):
		$Sound10.stop()	
	if (sound100_timer==0):
		$Sound100.stop()