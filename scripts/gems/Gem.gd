#Made by Xzing, project Tifias
extends Area2D

#a gem wich can give any number of points to the player
#the sprite need to be changed manually though

signal loot(points)

export(int) var points #NEED A GEMS_PARENT

var mvfe

func _ready():
	mvfe=self.connect("loot", get_parent(),"loot_gem")
	
func _on_Area2D_body_entered(body):
	self.emit_signal("loot", points)
	mvfe=body
	queue_free()