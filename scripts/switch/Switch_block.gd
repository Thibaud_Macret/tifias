#Made by Xzing, project Tifias
extends StaticBody2D

#a block wich turn between solid and non-solid form
#need a switch_parent

func translate_into_number(bool_to_translate):
	if (bool_to_translate):
		return 2
	else :
		return 1

func up_trigger(green_state):
	var etat = ($Sprite.frame)-27 #1 for G, 2 for W, 3 for innactive
	var num_state = translate_into_number(green_state)
	if (etat==3):
		$Sprite.frame=num_state+27
		_set_layers(1) 
	else :
		$Sprite.frame=30
		_set_layers(0)