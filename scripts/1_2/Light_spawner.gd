#Made by Xzing, project Tifias
extends StaticBody2D

#When the Start button is pressed, generate one light flux, the direction is pre-selected

export (int) var orientation

var flux = preload("res://prefabs/1_2/light_flux.tscn")

var spawned_in_this_scope = false
var mvfe

func _ready():
	gest_orientation()

func _physics_process(delta):
	if (Globals_1_2.is_playing && !spawned_in_this_scope):
		spawn_light()
	if (!Globals_1_2.is_playing && spawned_in_this_scope):
		spawned_in_this_scope = false
	mvfe = delta

func gest_orientation():
	if (orientation==-2):
		$Sprite.rotation_degrees=180
	if (abs(orientation)==1):
		$Sprite.rotation_degrees=90*orientation
	
func spawn_light(): #pretty much the same func as miror
	var new_flux = flux.instance()
	if (abs(orientation)==1):
		new_flux.init(global_position, 0, orientation, 20)
	else :
		new_flux.init(global_position, orientation/2, 0, 20)
	get_parent().call_deferred("add_child", new_flux)
	spawned_in_this_scope = true