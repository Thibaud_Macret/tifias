#Made by Xzing, project Tifias
extends StaticBody2D

#Miror : reflects light when game is "play" status
#if the game is in "stop" status, can be rotated by clicking on it
#Collisions : 4 areas, one for each direction : permit to knwo from where the mirror has been touched

export (int) var orientation #The shape : 1 for /   -1 for \   2 for -   -2 for |
export (bool) var activation_needed
var is_ok = true
var flux = preload("res://prefabs/1_2/light_flux.tscn")
var mvfe
var up_area_used= false
var down_area_used= false
var left_area_used= false
var right_area_used= false
var is_hovered = false

func _ready():
	set_pickable(true) #for mouse detection
	select_sprite()
	if(activation_needed):
		$Sprite.frame+=1

func select_sprite():
	if (abs(orientation)==1):
		$Sprite.frame = 5
	else:
		$Sprite.frame = 2
	check_sprite_orientation()
	if(activation_needed):
		$Sprite.frame+=1

func check_sprite_orientation():
	if (orientation<0):
		$Sprite.rotation_degrees=90
	else :
		$Sprite.rotation_degrees=0

func _physics_process(delta):
	if(Globals_1_2.is_playing): #When game is runing, gives it's status
		give_status()
	else :  #when game is paused, can be rotated by click
		reinit()
		if (Input.is_action_just_pressed("click")):
			if(is_hovered):
				change_orientation()
	mvfe=delta

func change_orientation():
	if (abs(orientation)==1):
		orientation*=2
	else :
		orientation/=-2
	select_sprite()

func reinit():
	if (is_ok):
		if (activation_needed):
			is_ok = false
			$Sprite.frame-=1
	up_area_used= false
	down_area_used= false
	left_area_used= false
	right_area_used= false

func give_status():
	if (!is_ok && Globals_1_2.is_level_completed):
		Globals_1_2.is_level_completed = false

func _on_Up_collider_area_exited(area):
	if(!up_area_used):
		up_area_used = generate_at(-up_down_dir_calc())
		gest_activation()
		mvfe=area

func _on_Down_collider_area_exited(area):
	if(!down_area_used):
		down_area_used = generate_at(up_down_dir_calc())
		gest_activation()
		mvfe=area

func up_down_dir_calc(): #- for up, + for down
	if (abs(orientation)==1):
		return (orientation*2)
	else :
		return (-orientation/2)

func _on_Left_collider_area_exited(area):
	if(!left_area_used):
		left_area_used = generate_at(orientation)
		gest_activation()
		mvfe=area

func _on_Right_collider_area_exited(area):
	if(!right_area_used):
		right_area_used = generate_at(-orientation)
		gest_activation()
		mvfe=area

func generate_at(dir): #dir = 1,2,-1,-2 for up,right,down,left
	var new_flux = flux.instance()
	if (abs(dir)==1):
		new_flux.init(global_position, 0, -dir, 3)
	else :
		new_flux.init(global_position, dir/2, 0, 3)
	get_parent().call_deferred("add_child", new_flux)
	return true #for the area activated bool

func gest_activation():
	if(activation_needed && !is_ok):
		$Sprite.frame+=1
		is_ok = true

func _on_Mouse_detect_mouse_entered():
	is_hovered=true

func _on_Mouse_detect_mouse_exited():
	is_hovered=false