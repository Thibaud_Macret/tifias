#Made by Xzing, project Tifias
extends StaticBody2D

#Launch the active part of the game : lights and end level check

var is_hovered = false
var mvfe

func _ready():
	set_pickable(true) #for mouse detection

func _physics_process(delta):
	if (Input.is_action_just_pressed("click")):
		if(is_hovered):
			Globals_1_2.is_playing = true
	select_sprite()
	mvfe=delta

func _on_Start_button_mouse_entered():
	is_hovered=true

func _on_Start_button_mouse_exited():
	is_hovered=false

func select_sprite():
	if (Globals_1_2.is_playing):
		$Sprite.frame = 1 #grey
	else :
		$Sprite.frame = 0 #white