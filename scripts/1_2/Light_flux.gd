#Made by Xzing, project Tifias
extends KinematicBody2D

#The base for light : a white dot wich let a little white mark behind him

const up =  Vector2(0,-1) #because move_and_slide()
export var move_vect = Vector2()
var light_mark = preload("res://prefabs/1_2/light_mark.tscn")
var mvfe

func init(parent_pos, x_dir, y_dir, first_mvmt):
	var spawn_pos = Vector2()
	move_vect.x = x_dir
	move_vect.y = y_dir
	#once created, the dot is instantly moved out of the collision box (otherwise it would instantly die...)
	spawn_pos.x = parent_pos.x + (x_dir * first_mvmt) 
	spawn_pos.y = parent_pos.y + (y_dir * first_mvmt)
	set_global_position(spawn_pos)

func _physics_process(delta):
	if(Globals_1_2.is_playing):
		move_loop()
		make_mark()
		mvfe=delta
	else :
		call_deferred('free') #the call is deferred for more stability

func move_loop():
	for pixel in range(1,64):
		move_vect=move_and_slide(move_vect,up)
		mvfe = pixel

func make_mark():
	var mark = light_mark.instance()
	mark.start(global_position)
	get_parent().add_child(mark)

func _on_Hitbox_ext_area_entered(area):
	queue_free()
	mvfe = area