#Made by Xzing, project Tifias
extends Area2D

#Light detecor code : detects if it has been touched by a light

var mvfe
var actived = false
var initialized = false
var is_ok #the value used to check if the detector is in the correct state
export (bool) var needed #if true, needs to be enlighten, if false it need to be not enlighten
export (bool) var externe #for sprite

func _physics_process(delta):
	if(Globals_1_2.is_playing):
		initialized = false
		give_status()
	else :
		init()
	mvfe = delta

func init():	
	if (!initialized):
		sprite_select()
		actived = false
		initialized = true

func sprite_select():
	if(needed):
		$Sprite.frame = 8
		is_ok = false
	else :
		$Sprite.frame = 10
		is_ok = true
	if(externe): #extern blocks have an orange background
		$Sprite.frame+=6

func give_status():
	if (!is_ok && Globals_1_2.is_level_completed):
		Globals_1_2.is_level_completed = false

func _on_light_detecor_body_entered(body):
	if (!actived):
		$Sprite.frame+=1
		actived = true
		is_ok = (!is_ok)
		mvfe = body