#Made by Xzing, project Tifias
extends Area2D

#Much like a duplicator in fact : when hit by a light, creates 4 light_flux : one per side

var mvfe
var actived = false
var initialized = false

var flux = preload("res://prefabs/1_2/light_flux.tscn")

func _physics_process(delta):
	if(Globals_1_2.is_playing):
		initialized = false
	else :
		init()
	mvfe = delta

func init():	
	if (!initialized):
		actived = false
		initialized = true
	
func _on_Light_separator_area_exited(area):
	if (!actived):
		actived = true
		generate_at(1)
		generate_at(-1)
		generate_at(-2)
		generate_at(2)
		mvfe = area
#possible improvement : not generating the light from where it came

func generate_at(dir): #this is nearly the same as the miror func
	var new_flux = flux.instance()
	if (abs(dir)==1):
		new_flux.init(global_position, 0, -dir, 6)
	else :
		new_flux.init(global_position, dir/2, 0, 6)
	get_parent().call_deferred("add_child", new_flux) #deferred for a clear queue