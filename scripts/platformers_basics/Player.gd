#Made by Xzing, project Tifias
extends KinematicBody2D

#The player node

const up =  Vector2(0,-1)
var vel = Vector2()
var sauts_restants = 0
var is_dying = false
var is_sprinting = false
var death_animation_timer=0.6
var checkpoint_x = -50 #fixed to a basic value avoids glitching...
var checkpoint_y = -50
#The proprieties :
const gravite = player_stats.gravite
var speed = player_stats.speed
var jump_power = player_stats.jump_power
var nb_sauts = player_stats.nb_sauts

func _physics_process(delta):
	gestion_anim()
	if (is_dying):
		death_gestion(delta)
		vel.x=0
		vel.y=gravite/2.0
	else :
		boucle_mouvements(delta)
		gest_sprint(delta)
	vel=move_and_slide(vel, up)

func boucle_mouvements(delta):
	#init input vars
	var dirdroite = Input.is_action_pressed("ui_right")
	var dirgauche = Input.is_action_pressed("ui_left")
	var dirx = int(dirdroite) - int(dirgauche)
	gestion_sauts()
	gestion_deplacements(dirx)
	vel.y =min(vel.y+gravite*delta,gravite) #min is used to smooth the fall
	
func gestion_sauts():
	if is_on_floor(): #give back the jumps
		sauts_restants = nb_sauts
	if (Input.is_action_just_pressed("ui_up")):
		if (sauts_restants>0):
			sauts_restants -= 1
			vel.y = jump_power
	if is_on_ceiling(): #when ceiling is touched, push the player back in order to look natural
		vel.y=100

func gestion_deplacements(dirx):
	if dirx==-1:
		left_move()
	elif dirx==1 :
		right_move()
	else : #if no button pressed, stops the player
		stop()

func left_move():
	vel.x = -speed
	$Sprite.flip_h=true

func right_move():
	vel.x = speed
	$Sprite.flip_h=false

func stop():
	if(is_on_floor()): #direct stop on floor
		vel.x=0
	else : #take some time in the air
		vel.x*=.95
		if abs(vel.x)<1:
			vel.x=0

func gestion_anim():
	if(is_dying):
		$Anim.play("Player_death")
	elif (is_on_floor()):
		floor_anim()
	else:
		air_anim()

func floor_anim():
	if (vel.x==0||is_on_wall()):
		$Anim.play("Player_iddle")
	elif (is_sprinting):
		$Anim.play("Player_run")
	else :
		$Anim.play("Player_walk")

func air_anim():
	if(vel.y<0 && sauts_restants!=nb_sauts): #if jumping
		if (sauts_restants==nb_sauts-1): #first jump is different
			$Anim.play("Player_jump_1")
		else :
			$Anim.play("Player_jump_2")
	elif(vel.y<0):
		$Anim.play("Player_walk")
	elif($Anim.get_current_animation()!="Player_jump_1" 
	&& $Anim.get_current_animation()!="Player_jump_2"): #the falling does not stops jumps animations
		$Anim.play("Player_fall")

func gest_sprint(delta):
	gest_sprint_input()
	if(is_sprinting):
		speed=player_stats.speed*2
		if (vel.x):
			player_stats.energy -= 256*delta #if the player isn't moving, he do not lose stamina
	else :
		speed=player_stats.speed

func gest_sprint_input():
	if(Input.is_action_pressed("sprint")&&is_on_floor()&&(player_stats.energy>50)):
		is_sprinting = true
	if!(Input.is_action_pressed("sprint")&&is_on_floor()&&(player_stats.energy>0)): #no energy/not sprinting/in the air = no sprint
		is_sprinting = false

func _on_Danger_parent_death():
	is_dying=true

func death_gestion(delta):
	if(death_animation_timer>0):
		death_animation_timer-=delta
	else: #when death animation is finished, the player is moved to the last cp position
		is_dying=false
		death_animation_timer=0.6
		position.x=checkpoint_x
		position.y=checkpoint_y
		player_stats.energy = player_stats.max_energy

func _on_Checkpoint_parent_cp_valid(pos_x, pos_y):
	checkpoint_x=pos_x
	checkpoint_y=pos_y