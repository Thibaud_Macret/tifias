#Made by Xzing, project Tifias
extends Node2D

#The bar wich indicate the energy of the player

var pixel = preload("res://prefabs/bars/pixel.tscn")
var bar_shape_ext = preload("res://prefabs/bars/bars_shape_ext.tscn")
var bar_shape_int = preload("res://prefabs/bars/bars_shape_int.tscn")
var energy_regen = player_stats.energy_regen
var max_energy = player_stats.max_energy
var body_size

func _ready():
	build_bar()

func build_bar():
	body_size = calculate_body_size()
	var building_x = position.x
	var building_y = position.y
	build_borders(building_x,building_y)
	build_body(building_x,building_y)
	full_bar(building_x-14,building_y-6)

func calculate_body_size():
	#nb_pixels = max_energy*2
	return ((max_energy-60)/32)+1 #-60 beacause of the borders and

func build_borders(x,y):
	make_border(x,y,false)
	x += 32*body_size
	make_border(x,y,true)

func make_border(x,y,flip):
	var sprite = bar_shape_ext.instance()
	sprite.init(x,y)
	if (flip):
		sprite.flip()
	self.add_child(sprite)

func build_body(x,y):
	var x_pos = x
	while(body_size):
		x_pos+=32 #increments before, because the first pos is the border one
		var sprite = bar_shape_int.instance()
		sprite.init(x_pos,y)
		self.add_child(sprite)
		body_size-=1

func full_bar(x,y):
	for index in range(max_energy-1):
		make_pixel(x+index,y,index)

func make_pixel(x,y,rank):
	var sprite = pixel.instance()
	sprite.init(x,y,rank)
	self.add_child(sprite)

func _physics_process(delta):
	actu_val(delta)

func actu_val(delta):
	if(player_stats.energy<max_energy):
		player_stats.energy += 60*energy_regen*delta