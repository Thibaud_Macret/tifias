#Made by Xzing, project Tifias
extends Area2D

#Checkpoint for platformer : when touched by the player, emit a signal

signal cp_valid(x,y)

var mvfe

func _ready():
	mvfe=self.connect("cp_valid",get_parent(),"cp_valid")

func _on_Checkpoint_body_entered(body):
	$Sprite.frame=1 #While is the activecheckpoint, frame is different
	self.emit_signal("cp_valid",position.x,position.y)
	mvfe=body